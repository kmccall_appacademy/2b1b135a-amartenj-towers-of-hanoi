class TowersOfHanoi
  require 'byebug'

  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    while not won?
      render

      print "Starting tower: "
      from = gets.chomp.to_i
      print "Ending tower: "
      to = gets.chomp.to_i

      if legal_input?(from, to)
        if valid_move?(from, to)
          move(from, to)
        else
          puts "You cannot move there"
        end
      else
        puts "That is not valid input. Input must be 0, 1 or 2 for both towers"
      end
    end
    puts "Game over"
  end

  def legal_input?(from, to)
    valid_moves = [0, 1, 2]
    valid_moves.include?(from) && valid_moves.include?(to)
  end

  def valid_move?(from, to)
    return false if @towers[from].empty?
    @towers[to].empty? || @towers[from].last < @towers[to].last
  end

  def move(from, to)
    @towers[to].push(@towers[from].pop) if valid_move?(from, to)
  end

  def won?
    (@towers[1] == [3, 2, 1]) != (@towers[2] == [3, 2, 1])
  end

  def render
    idx = 2
    while idx >= 0
      row = []
      @towers.each { |tower| tower[idx].nil? ? row << "_" : row << tower[idx] }
      puts row.join(" ")
      idx -= 1
    end
  end
end

game = TowersOfHanoi.new
game.play
